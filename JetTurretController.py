import math
import threading
import time
import copy
from collections import deque

import serial
import numpy as np
import yaml

from dataclass import RobotStatus, MarkerActionInfo, TurretParam


class JetTurretController:
    """ jet turret 제어기.

        요청을 보내고 결과를 받는 방식으로 구성된다.
    """
    b_stop = False
    b_running = False

    tilt_angles = {"high": 180, "mid": 140, "low": 120}
    fan_speed = {"high": 200, "mid": 170, "low": 125, "off": 0}
    jet_actions = {}
    param: TurretParam = {}
    max_pan_speed = 70

    def __init__(self, serial_port: str, param: TurretParam, simulation=False):
        self.simulation = simulation
        if not self.simulation:
            self.serial_io = serial.Serial(serial_port, baudrate=57600, parity='N', stopbits=1, bytesize=8, timeout=15)
            print(self.serial_io.readline())
            self.read_thread = threading.Thread(target=self.run)
        #        self.read_thread.start()
        self.set_param(param)
        self.load_jet_actions()
        # print(self.jet_actions)

    def stop_all(self):
        self.control_fan('off')
        self.control_pump('off')

    def load_jet_actions(self):
        try:
            with open(f"conf/jet_action.yaml", encoding="UTF8") as f:
                self.jet_actions = yaml.full_load(f)
                f.close()
        except Exception as e:
            print(e)

    def set_param(self, param: TurretParam):
        self.param = param
        self.max_pan_speed = param.max_pan_speed
        self.fan_speed['low'] = param.fan_low_value
        self.fan_speed['high'] = param.fan_high_value
        self.tilt_angles['high'] = high = param.tilt_high_value
        self.tilt_angles['mid'] = param.tilt_mid_value
        self.tilt_angles['low'] = low = param.tilt_low_value
        if not self.simulation:
            self.serial_io.write(f"set-tilt-range {low} {high}".encode())

    def run(self):
        self.serial_io.readline()
        self.b_stop = False
        while True:
            try:
                message = self.serial_io.readline()
                # message = message.decode('utf-8').strip()
                print(message)
            except:
                print("read error")
            self.b_running = True
            # time.sleep(0.01)

            if self.b_stop:
                break

    def marker_action(self, marker_id: int):
        jet_action = self.jet_actions[marker_id]
        ret = self.control_turret(jet_action['turret'][0])
        print(ret)
        ret = self.control_tilt(jet_action['tilt'])
        print(ret)
        ret = self.control_fan(jet_action['fan'])
        print(ret)
        ret = self.control_pump(jet_action['pump'])
        print(ret)

        if jet_action['wait']:
            print(f"sleep={self.param.jet_wait_time}")
            time.sleep(self.param.jet_wait_time)

        print(f"marker action end")

    def get_marker_action(self, marker_id):
        if marker_id not in self.jet_actions:
            return None
        dict = {}
        dict['marker_id'] = marker_id
        jet_action = self.jet_actions[marker_id]
        if jet_action['pump'] == 'on':
            dict['spray'] = '소독액 분사'
        else:
            dict['spray'] = '소독액 분사 안함'

        if jet_action['fan'] == 'mid':
            dict['fan_speed'] = '강함'
        elif jet_action['fan'] == 'low':
            dict['fan_speed'] = '약함'
        else:
            dict['fan_speed'] = '매우강함'

        if jet_action['tilt'] == 'low':
            dict['vertical'] = '낮음'
        else:
            dict['vertical'] = '높음'

        if jet_action['turret'][0] == 'left':
            dict['horizontal'] = '왼쪽'
        elif jet_action['turret'][0] == 'right':
            dict['horizontal'] = '오른쪽'
        elif jet_action['turret'][0] == 'front':
            dict['horizontal'] = '정면'

        dict['wait'] = jet_action['wait']

        return MarkerActionInfo(**dict)

        # self.control_tilt(tilt_cmd)
        # self.control_fan(fan_cmd)
        # self.control_pump(pump_cmd)

    def control_pump(self, cmd):
        if cmd == "on" and not self.simulation:
            self.serial_io.write(f"pump-on".encode())
        elif cmd == "off" and not self.simulation:
            self.serial_io.write(f"pump-off".encode())
        if not self.simulation:
            return self.serial_io.readline().decode("utf-8")
        else:
            return cmd

    def control_fan(self, cmd: str):
        if cmd == "low" and not self.simulation:
            self.serial_io.write(f"fan 200".encode())
            time.sleep(2)
        if not self.simulation:
            self.serial_io.write(f"fan {self.fan_speed[cmd]}".encode())

        if not self.simulation:
            return self.serial_io.readline().decode("utf-8")
        else:
            return cmd

    def control_tilt(self, cmd: str):

        '''
        "분사구의 수직방향을 조절한다. "
        :param cmd: repeat, high, mid, low가 있음
        :return:
        '''
        if not self.simulation:
            if cmd == "repeat":
                self.serial_io.write(f"jet-tilt-repeat".encode())
            else:
                self.serial_io.write(f"jet-tilt {self.tilt_angles[cmd]}".encode())
        if not self.simulation:
            return self.serial_io.readline().decode("utf-8")
        else:
            return cmd

    def control_turret(self, cmd: str, speed=None):
        if not speed:
            speed = self.max_pan_speed
        '''
        터렛의 방향을 결정한다. 명령어 left, front, right
        :param cmd:
        :return:
        '''
        if not self.simulation:
            self.serial_io.write(f"turret-{cmd} {speed}".encode())

        if not self.simulation:
            return self.serial_io.readline().decode("utf-8")
        else:
            return f"turret-{cmd} {speed}".encode()

    def control_turret_angle(self, relative_angle: float, speed=None):
        if not speed:
            speed = self.max_pan_speed
        '''
        터렛을 회전시킨다. 
        :param relative_angle: 움직일 각도 
        :param speed: 속도 30~50정도가 적당
        :return: 
        '''
        if not self.simulation:
            self.serial_io.write(f"turret-turn {relative_angle} {speed}".encode())

        if not self.simulation:
            return self.serial_io.readline().decode("utf-8")
        else:
            return f"turret-turn {relative_angle} {speed}"


if __name__ == "__main__":
    controller = JetTurretController(serial_port="COM5")
    controller.marker_action(5)
    time.sleep(5)
    controller.marker_action(1)
    #    print(controller.pump_on())
    # time(2)
    # print(controller.pump_off())
    #
    # print(controller.control_fan(200))
    # time(2)
    # print(controller.control_fan(0))
    #
    # print(controller.control_led(200))
    # time(2)
    # print(controller.control_tilt("high"))
    # time.sleep(2)
    # print(controller.control_tilt("mid"))
    # time.sleep(2)
    # print(controller.control_tilt("low"))
    # time.sleep(2)
    # print(controller.control_turret(30,20))
    # time(2)
    # print(controller.control_turret(-30,20))

#    while True:
#        print(drive.get_status())
#        time.sleep(0.1)
