import logging

log_level = logging.INFO
logging.basicConfig(format='%(asctime)s,%(msecs)03d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
                    datefmt='%Y-%m-%d:%H:%M:%S',
                    level=logging.INFO)


def get_logger(name):
    logger = logging.getLogger(name=name)
    return logger
