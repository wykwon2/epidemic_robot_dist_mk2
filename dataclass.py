from dataclasses import dataclass
from typing import Union, Dict, List, Optional, Literal
from pydantic import BaseModel, Field
from enum import Enum, auto

########################################################################
# Display 자료구조
########################################################################

from strenum import StrEnum

class ERROR(StrEnum):
    obstacle = '장애물회피 실패'
    marker = '마커인식 실패'
    unknown = '알수없는 오류'
    battery = '배터리 부족'
    conflict = '중복된 작업요청 오류'
    liquid='소독액 부족'
    network = '네트워크 오류'
    stop = '비상정지'

class TASK_STATE(StrEnum):
    init = '초기화 중'
    ready = '대기(소독가능)'
    running = '소독중'
    complete = '소독완료'
    action_complete = '동작완료'
    driving = '구동부 제어중'


class LIQUID(StrEnum):
    normal = "정상"
    low = '부족'


class BATTERY(StrEnum):
    high = "높음"
    mid = "정상"
    low = "낮음"

class MobileDisplayData(BaseModel):
    robot_serial: str = Field(..., description='로봇 시리얼')
    battery_level: str = Field(..., description='배터리 레벨')
    battery_percent: float = Field(..., description='배터리 잔여량 퍼센트')
    liquid_level: str = Field(..., description='소독액 상태')
    task_status: str = Field(..., description='작업상태')
    temperature: float = Field(..., description='온도')
    humidity: float  = Field(..., description='습도%')

@dataclass
class DisplayData():
    battery: BATTERY
    liquid: LIQUID
    task_status: TASK_STATE
    task_message: Optional[str] = None
    debug_message: Optional[str] = None


########################################################################
# DB접근 자료구조
#########################################################################
class UserItem(BaseModel):
    email: str = Field(..., description='사용자 idst')
    password: str = Field(..., description='비밀번호 ')
    name: str = Field(..., description='사용자 idst')
    type: str = Field(..., description="사용자 타입, 관리자인 admin과 현장운영자인 operator로 구분됨")
    org: str = Field(..., description="사용자 소속")


class RobotItem(BaseModel):
    serial: str = Field(..., description='로봇 제조번호')
    name: str = Field(..., description='로봇이름')
    date: str = Field(..., description='제조일')
    operator: Optional[str] = None
    site: Optional[str] = None


########################################################################
# 마이크로서비스간 자료구조
#########################################################################
class MarkerPose(BaseModel):
    """ARUCO 마커의 world 좌표계에서의 자세 정보"""
    marker_id: int = Field(..., description="인식된 마커의 id")
    dist: Optional[str] = Field(..., description="마커와의 거리 (far, near, unknown)")
    x: float = Field(..., description="로봇기준으로 인식된 마커의 x좌표 [단위 cm]")
    y: float = Field(..., description="로봇기준으로 인식된 마커의 y좌표 [단위 cm]")
    r: float = Field(..., description="로봇기준으로 인식된 마커와의 거리[단위 cm]")
    theta: float = Field(..., description="로봇정면과 인식된 마커와의 각도[단위 degree]")
    yaw: float = Field(..., description="인식된 마커의 yaw 각도 [단위 degree]")


class MarkerRegInfo(BaseModel):
    camera_id: int = Field(..., description="사용할 카메라 id")
    marker_id: int = Field(..., description="등록할 마커의 id")
    offset_x: float = Field(..., description="등록할 마커와 등록물체와의 x거리 (정면)[단위 cm]")
    offset_y: float = Field(..., description="등록할 마커와 등록물체와의 y거리 (정면)[단위 cm]")
    marker_length: float = Field(..., description="등록할 마커의 한쪽 면 길이[단위 cm]")


class MarkerInfo(BaseModel):
    """ARUCO 마커의 화면상 인식정보"""
    marker_id: int
    top_left: List[int]
    top_right: List[int]
    bottom_right: List[int]
    bottom_left: List[int]


class DeviceStatus(BaseModel):
    """로봇의 구동부 상태를 반환함 """
    g_x: float = Field(..., description="로봇의 전역 위치 x좌표 [단위 cm]")
    g_y: float = Field(..., description="로봇의 전역 위치 y좌표 [단위 cm]")
    g_heading: float = Field(..., description="로봇의 전역 방향 [단위 degree]")
    l_x: float = Field(..., description="위치 리셋시부터 누적된 로봇의  x좌표 [단위 cm]")
    l_y: float = Field(..., description="위치 리셋시부터 누적된 로봇의  y좌표 [단위 cm]")
    l_heading: float = Field(..., description="위치 리셋시부터 누적된 로봇의 방향 [단위 degree]")
    l_vel: float = Field(..., description="로봇의 바퀴 선속도 [단위 cm/s]")
    a_vel: float = Field(..., description="로봇의 바퀴 각속도 [단위 cm/s]")
    battery: float = Field(..., description="로봇의 배터리 전압 [단위 V]")
    error: str = Field(..., description="로봇의 오류코드. 빈 문자열이면 오류가 없음 ")


class TaskStatus(BaseModel):
    """방역로봇 작업상황 알림"""
    action: str = Field(..., description="로봇이 현재 수행중인 동작. 없으면 'ready'")
    battery: str = Field(..., description="배터리 상태. 0, 25, 50, 75, 100중 하나 ")
    error: str = Field(..., description="로봇의 오류상황(없으면 빈 문자열) ")


class MoveCmd(BaseModel):
    """로봇의 전진 또는 후진 명령"""
    distance: float = Field(..., description="이동거리(+면 전진, -면 후진)[단위 cm]")
    speed: Optional[float] = None
    # Field(..., description="최대 이동속도 [단위 cm/s]")


class TurnCmd(BaseModel):
    """로봇의 회전명령 """
    degree: float = Field(..., description="회전각도 [단위 cm]")
    speed: Optional[float] = None
    # Field(..., description="최대 이동속도 [단위 cm/s]")


class TurretCmd(BaseModel):
    """로봇의 회전명령 """
    cmd: Literal['init', 'front', 'left', 'right'] = Field(..., description="터렛 회전명령")
    speed: Optional[int] = None
    # Field(..., description="터렛의 회전속도 30~50권장")

class DriveParam(BaseModel):
    serial: Optional[str] = None
    max_l_speed: float = Field(..., description="최대선속도 [단위 cm/s]")
    max_a_speed: float = Field(..., description="최대각속도 [단위 degree/s]")
    proximity_dist: int = Field(..., description="장애물 감지범위 0~5000 이며, 1000을 기본값으로 낮을수록 먼거리 장애물 인식")
    move_step: float = Field(..., description="전진시 이동거리")
    turn_step: float = Field(..., description="구동부 회전시 이동각도")
    battery_good_value: float = Field(..., description="배터리 high일 경우의 센서측정값 하한선")
    battery_normal_value: float = Field(..., description="배터리 mid 일 경우의 센서측정값 하한선")
    sensing_interval:float  = Field(..., description="로봇 구동부 센싱 주기(초)")

class TurretParam(BaseModel):
    serial: Optional[str] = None
    max_pan_speed: float = Field(..., description="터렛 수평 회전속도 [단위 degree/s]")
    tilt_high_value: int = Field(..., description="분사구 수직방향 high일때의 서보값")
    tilt_mid_value: int = Field(..., description="분사구 수직방향 mid일때의 서보값")
    tilt_low_value: int = Field(..., description="분사구 수직방향 low(수평)일때의 서보값")
    fan_high_value: int = Field(..., description="fan high일때의 pwm 값")
    fan_low_value: int = Field(..., description="fan low일때의 pwm 값")
    jet_wait_time: int = Field(..., description="wait가 true일때 분사하면서 대기하는 시간")
    turn_step: float = Field(..., description="터렛 회전시 이동각도")


# #depricated => DriveParam
# class LidarParam(BaseModel):
#     """ARUCO 마커의 화면상 인식정보"""
#     use_lidar: bool = Field(..., description="전진시 장애물 감지 사용여부 ")
#     front_range: float = Field(..., description="전면 장애물 감지범위 [단위 cm]")
#     side_range: float = Field(..., description="측면 장애물 감지범위 [단위 cm]")
#     sensitivity: int = Field(..., description="민감도(처리할 장애물 감지 포인트 갯수)")
# #depricated => DriveParam
# class WheelParam(BaseModel):
#     max_l_speed: float = Field(..., description="최대선속도 [단위 cm/s]")
#     max_a_speed: float = Field(..., description="최대각속도 [단위 degree/s]")


class DriveStatus(BaseModel):
    """로봇이 Move 동작을 하는중의 상태를 반환함  """
    status: str = Field(..., description="로봇이 현재 수행하는 명령을 나타내며, ready인 경우 대기상태(move, turn, ready)")
    time: float = Field(..., description="구동명령 이후 지난 시간[sec]")
    target: float = Field(..., description="목표한 거리 또는 각도 [단위 cm, degree]")
    travel: float = Field(..., description="움직인 거리 또는 각도 [단위 cm, degree]")
    l_x: float = Field(..., description="위치 리셋시부터 누적된 로봇의  x좌표 [단위 cm]")
    l_y: float = Field(..., description="위치 리셋시부터 누적된 로봇의  y좌표 [단위 cm]")
    l_heading: float = Field(..., description="위치 리셋시부터 누적된 로봇의 방향 [단위 degree]")
    l_vel: float = Field(..., description="로봇의 바퀴 선속도 [단위 cm/s]")
    a_vel: float = Field(..., description="로봇의 바퀴 각속도 [단위 cm/s]")
    battery: float=  Field(..., description="배터리 레벨, 0~1024 ")
    l_dist:float = Field(..., description="전면 왼쪽 장애물 센서 측정값, 0~5000. 값이 클수록 가까움")
    f_dist: float = Field(..., description="전면 가운데 장애물 센서 측정값, 0~5000. 값이 클수록 가까움")
    r_dist: float = Field(..., description="전면 오르쪽 장애물 센서 측정값, 0~5000. 값이 클수록 가까움")
    error: Optional[str] = None
    msg: Optional[str] = None


class MarkerActionInfo(BaseModel):
    marker_id: int = Field(..., description="분사 작업대상 마커의 ID")
    spray: str = Field(..., description="분사작업여부")
    fan_speed: str = Field(..., description="분사팬 강도")
    horizontal: str = Field(..., description="분사부 좌우")
    vertical: str = Field(..., description="분사부 상하")
    wait: bool = Field(..., description="분사 대기")


class JetAction(BaseModel):
    marker_id: int = Field(..., description="분사 작업대상 마커의 ID")
    spray: str = Field(..., description="분사작업여부")
    fan_speed: str = Field(..., description="분사팬 강도")
    horizontal: str = Field(..., description="분사부 좌우")
    vertical: str = Field(..., description="분사부 상하")
    wait_time: float = Field(..., description="분사 대기시간")


class NavigationStatus(BaseModel):
    """로봇이 마커기반 자율주행 동작을 하는중의 상태를 반환함  """
    time: float = Field(..., description="구동명령 이후 지난 시간[sec]")
    status: str = Field(..., description="로봇이 현재 수행하는 자율주행 명령을 나타냄, approach, search, align")
    error: str = Field(..., description="에러 메시지, 에러가 없을경우 빈 문자열. 현재 no_marker,not_close_marker, obstacle 가 존재함")
    msg: str = Field(..., description="마커기반 주행 메시지")
    marker_pose: Optional[MarkerPose] = None
    drive_status: Optional[DriveStatus] = None
    marker_action: Optional[MarkerActionInfo] = None


class NavigationParam(BaseModel):
    serial: Optional[str] = None
    """마커기반 로봇 자율주행을 위한 파라메터들"""
    delta: float = Field(..., description="마커의 탐색 및 마커에 접근할 경우 한번의 이동거리 ")
    min_marker_dist: float = Field(..., description="카메라가 마커를 인식할수 있는 최소거리 ")
    max_marker_dist: float = Field(..., description="카메라가 마커를 인식할수 있는 최대거리 ")
    theta_s: float = Field(..., description="마커탐색시 좌우로 회전하는 각도 ")
    epsilon: float = Field(..., description="마커의 거리인식시 변동을 반영하기 위한 거리 여유분")



#    l_speed: float = Field(..., description="구동부 이동시 선속도(최대)")
#    a_speed: float = Field(..., description="구동부 회전시 각속도(최대)")


########################################################################
# 로봇 데이터 중계서버용 자료구조
#########################################################################
class RobotStatus(BaseModel):
    timestamp: int = Field(..., description="로봇 상태데이터가 생성된 시간")
    # curr_time = round(time.time()*1000)
    serial: str = Field(..., description="로봇의 serial 번호")
    drive_status: Optional[DriveStatus] = None
    navigation_status: Optional[NavigationStatus] = None
    marker_pose: Optional[MarkerPose] = None
    stream_end: Optional[str] = None
    # status: Optional[str] = None


class RobotCmd(BaseModel):
    robot_serial: str = Field(..., description="로봇 고유번호")
    command: str = Field(..., description="명령어")
    params: Optional[List[float]] = None
    args: Optional[List[str]] = None
