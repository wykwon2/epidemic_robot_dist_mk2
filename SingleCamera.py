import threading, time, sys, cv2, platform
from threading import Thread

from logger_config import get_logger

logger = get_logger(__name__)


class SingleCamera(threading.Thread):
    camera: any = None
    cur_image: any
    _thread: Thread

    def __init__(self, width=1920, height=1080, delay_sec=0.01):
        self._has_image = False
        self._running = True
        self.delay_sec = delay_sec;
        self.width = width;
        self.height = height;

    def start(self):
        cam_id = 0
        if platform.system() == 'Windows':
            self.camera = cv2.VideoCapture(cam_id,cv2.CAP_DSHOW)
        else:
            self.camera = cv2.VideoCapture(cam_id)

        self.camera.set(cv2.CAP_PROP_FRAME_WIDTH, self.width)
        self.camera.set(cv2.CAP_PROP_FRAME_HEIGHT, self.height)
        self.camera.set(cv2.CAP_PROP_AUTOFOCUS, 0)
        self._running = True
        self._thread = threading.Thread(target=self.run)
        self._thread.start()
        for i in range(0, 10):
            if self._has_image:
                return
            time.sleep(1)


    def stop(self):
        self._running = False
        self._thread.join()

    def run(self):
        while self._running is True:
            status, img = self.camera.read()
            if status:
                self.cur_image = img
                self._has_image = True
            time.sleep(self.delay_sec)

    def get_frame(self):
        return self.cur_image

    def get_png_buffer(self):
        img = self.get_frame()
        if img is not None:
            return cv2.imencode('.png', img)[1].tobytes()
        else:
            return None

    # def __del__(self):
    #     self.camera.release()


def main(argv):
    logger.debug("init cameras")

    cam = SingleCamera()
    cam.start()

    while True:
        logger.debug(cam.get_frame())
        cv2.imshow("cam", cam.get_frame())

        if cv2.waitKey(10) & 0xFF == ord('q'):
            break

    cv2.destroyAllWindows()


if __name__ == "__main__":
    main(sys.argv)

    # cameras = DoubleCamera()
    # while cv2.waitKey(33) < 0:
    #     frame = cameras.get_frame2()
    #     logger.debug(e)(frame)
    # cv2.imshow("VideoFrame", frame)
