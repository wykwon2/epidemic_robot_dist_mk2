import logging
import platform

import cv2
import re
import sys, os, asyncio, time, requests, zmq, yaml
from ConfigManager import ConfigManager
from EpidemicRobotController import EpidemicRobotController
from threading import Thread

from RobotDisplay import RobotDisplay
from dataclass import RobotCmd, DriveParam, TurretParam, MarkerPose, DriveStatus, NavigationStatus, NavigationParam, \
    RobotStatus, DisplayData, BATTERY, LIQUID, TASK_STATE, MobileDisplayData, ERROR
from logger_config import get_logger

if platform.machine() in ('aarch64', 'armv7l'):
    from gpiozero import Button
    from gpiozero.pins.pigpio import PiGPIOFactory
    import Adafruit_DHT as dht
    import RPi.GPIO as GPIO

logger = get_logger(__name__)
global_display_data = DisplayData(battery=BATTERY.mid, liquid=LIQUID.normal, task_status=TASK_STATE.init,
                                  task_message='연결을 시도하고 있습니다.')

display = RobotDisplay(data=global_display_data)

batch_task_by_button = False


class RobotClient:
    _cmd_recv_thread: Thread
    config = ConfigManager()
    mobile_display_data: MobileDisplayData = None
    liquid_level = '정상'
    temperature = 11.0
    humidity = 11.0
    task_status = '대기(소독가능)'
    liquid_level = '정상'
    last_robot_status:RobotStatus=None

    def __init__(self, conf_file='conf/client_conf.yaml', simulation=False):
        logger.info(f'Initializing robot with simulation mode={simulation}')
        with open(f"conf/local_conf.yaml", encoding="UTF8") as f:
            dict = yaml.full_load(f)
            self.serial = dict['robot_serial']
            self.version = dict['version']
            f.close()

        self.config.load_config(conf_file)
        self.robot_controller = EpidemicRobotController(self.config, simulation=simulation)
        self._cmd_recv_thread = Thread(target=self.run_robot_task)
        self._button_task_thread = Thread(target=self.run_task_by_button)
        self._stop_cmd_recv_thread = Thread(target=self.run_stop_cmd_read)
        self._mobile_display_thread = Thread(target=self.run_mobile_display_data)
        self._sensor_read_thread = Thread(target=self.run_sensor_read)
        self.state_request_socket = zmq.Context().socket(zmq.REQ)

        addr = self.config.server_ports['ip_addr']
        port = self.config.server_ports['zmq_port']
        self.state_request_socket.connect(f'tcp://{addr}:{port}')
        self.state_request_socket.setsockopt(zmq.RCVTIMEO, 1000)
        self.state_request_socket.setsockopt(zmq.LINGER, 0)


        self._sensor_read_thread.start()
        self._cmd_recv_thread.start()
        self._stop_cmd_recv_thread.start()
        self._button_task_thread.start()
        self._mobile_display_thread.start()

        ## busy를 풀기위해 보냄
        logger.info("zmq socket connect")
        self.send_robot_status(stream_end='end')

    def run_sensor_read(self):
        logger.info("run_sensor_read_thread")
        liquid_sensor_pin = 11
        if platform.machine() in ('aarch64', 'armv7l'):
            GPIO.setwarnings(False)
            GPIO.setmode(GPIO.BCM)  # 핀모드 설정
            GPIO.setup(liquid_sensor_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        while True:
            if platform.machine() in ('aarch64', 'armv7l'):
                # logger.info("=================temp, humidity============")
                if GPIO.input(liquid_sensor_pin) == GPIO.HIGH:
                    liquid = '부족'
                else:
                    liquid = '정상'
                if self.liquid_level != liquid:
                    self.liquid_level = liquid
                    self.send_display_data()
                self.liquid_level = liquid


                humidity, temperature = dht.read_retry(dht.DHT22, 10)
                if humidity:
                    self.humidity = round(humidity,1)
                    # print(self.humidity)
                if temperature:
                    self.temperature = round(temperature,1)
                    # print(self.temperature)
                liquid = self.liquid_level
                logger.info(f"temp={temperature}, humidity={humidity}, liquid_level ={liquid} ")

            time.sleep(0.5)

    def run_mobile_display_data(self):
        while True:
            self.prepare_mobile_display_data(self.last_robot_status)
            if self.mobile_display_data:
                addr = self.config.server_ports['ip_addr']
                port = self.config.server_ports['rest_port']
                serial = self.config.robot_serial
                # logger.info(self.mobile_display_data)
                url = f'http://{addr}:{port}/set-mobile-display-data'
                logger.debug(url)
                requests.post(url, json=self.mobile_display_data.model_dump())

            time.sleep(1)

    def run_task_by_button(self):
        global batch_task_by_button
        while True:
            try:
                if batch_task_by_button:
                    batch_task_by_button = False
                    dict = {'command': 'batch-task', 'robot_serial': self.serial}
                    robot_cmd = RobotCmd(**dict)
                    logger.info(f"start task by button {robot_cmd}")
                    asyncio.run(self.handle_robot_cmd(robot_cmd))

                time.sleep(0.05)
            except Exception as e:
                logger.error(e)

    def run_stop_cmd_read(self):
        """서버로부터 로봇 제어 명령을 받아온후 실행함. 쓰레드 실행 """

        while True:
            try:
                addr = self.config.server_ports['ip_addr']
                port = self.config.server_ports['rest_port']
                serial = self.serial

                url = f'http://{addr}:{port}/get-robot-stop-cmd/{serial}'
                logger.debug(url)
                response = requests.get(url)
                response.raise_for_status()
                dict = response.json()
                logger.debug(dict)
                if dict is not None:
                    logger.debug(f'recv={dict}')
                    robot_cmd = RobotCmd(**dict)
                    logger.debug(robot_cmd)
                    if robot_cmd.command == 'stop-all':
                        self.robot_controller.stop_all()

            except Exception as e:
                logger.error(e)
                time.sleep(5)

    #            asyncio.run(self.handle_robot_cmd(robot_cmd))

    def run_robot_task(self):
        first_connect = False
        """서버로부터 로봇 제어 명령을 받아온후 실행함. 쓰레드 실행 """
        logger.info("Start thread getting robot command")
        while True:
            try:
                addr = self.config.server_ports['ip_addr']
                port = self.config.server_ports['rest_port']
                serial = self.serial
                version = self.version
                url = f'http://{addr}:{port}/get-robot-cmd/{serial}'
                logger.debug(url)

                ###             최초 display data를 보낼때는 long-polling의 timeout을 빠르게 한다.
                if not first_connect:
                    response = requests.get(url, params={'version': self.version, 'timeout': 1})
                    response.raise_for_status()
                    self.send_display_data()
                    first_connect = True
                else:
                    response = requests.get(url, params={'version': self.version})
                    response.raise_for_status()
                    dict = response.json()
                    if dict is not None:
                        logger.info(dict)
                        logger.debug(f'recv={dict}')
                        robot_cmd = RobotCmd(**dict)
                        asyncio.run(self.handle_robot_cmd(robot_cmd))

            except Exception as e:
                logger.error(e)
                time.sleep(1)

    #            asyncio.run(self.handle_robot_cmd(robot_cmd))

    async def handle_robot_cmd(self, robot_cmd: RobotCmd):
        logger.info(f'handle robot cmd={robot_cmd}')
        try:
            if robot_cmd.command == 'move':
                if len(robot_cmd.params) == 1:
                    async for event in self.robot_controller.diff_drive.move(robot_cmd.params[0]):
                        self.send_robot_status(drive_status=event)
                elif len(robot_cmd.params) == 2:
                    async for event in self.robot_controller.diff_drive.move(robot_cmd.params[0], robot_cmd.params[1]):
                        self.send_robot_status(drive_status=event)

            elif robot_cmd.command == 'turn':
                if len(robot_cmd.params) == 1:
                    async for event in self.robot_controller.diff_drive.turn(robot_cmd.params[0]):
                        # logger.info(event)
                        self.send_robot_status(drive_status=event)
                elif len(robot_cmd.params) == 2:
                    async for event in self.robot_controller.diff_drive.turn(robot_cmd.params[0], robot_cmd.params[1]):
                        # logger.info(event)
                        self.send_robot_status(drive_status=event)

            elif robot_cmd.command == 'batch-task':
                async for event in self.robot_controller.batch_task():
                    self.send_robot_status(navigation_status=event)

            elif robot_cmd.command == 'approach-marker':
                async for event in self.robot_controller.approach_marker(start_time=time.time(), as_subtask=False):
                    self.send_robot_status(navigation_status=event)


            elif robot_cmd.command == 'align-marker':
                async for event in self.robot_controller.align_marker(start_time=time.time(), as_subtask=False):
                    self.send_robot_status(navigation_status=event)

            elif robot_cmd.command == 'spray-marker':
                async for event in self.robot_controller.spray_marker():
                    self.send_robot_status(navigation_status=event)

            elif robot_cmd.command == 'search-marker':
                async for event in self.robot_controller.search_marker(start_time=time.time(), as_subtask=False):
                    self.send_robot_status(navigation_status=event)

            elif robot_cmd.command == 'refresh-params':
                self.refresh_params(robot_cmd.params[0])

            elif robot_cmd.command == 'request-marker-image':
                self.robot_controller.marker_detector.request_marker_image()
                self.robot_controller.export_marker_pose()

            elif robot_cmd.command == 'request-marker-pose':
                self.robot_controller.export_marker_pose()

            elif robot_cmd.command == 'tilt-high':
                self.robot_controller.jet_turret.control_tilt('high')

            elif robot_cmd.command == 'tilt-low':
                self.robot_controller.jet_turret.control_tilt('low')

            elif robot_cmd.command == 'tilt-repeat':
                self.robot_controller.jet_turret.control_tilt('repeat')

            elif robot_cmd.command == 'turret-left':
                self.robot_controller.jet_turret.control_turret('left')

            elif robot_cmd.command == 'turret-front':
                self.robot_controller.jet_turret.control_turret('front')

            elif robot_cmd.command == 'turret-right':
                self.robot_controller.jet_turret.control_turret('right')

            elif robot_cmd.command == 'turret-turn':
                self.robot_controller.jet_turret.control_turret_angle(robot_cmd.params[0])

            elif robot_cmd.command == 'fan-high':
                self.robot_controller.jet_turret.control_fan('high')

            elif robot_cmd.command == 'fan-mid':
                self.robot_controller.jet_turret.control_fan('mid')

            elif robot_cmd.command == 'fan-low':
                self.robot_controller.jet_turret.control_fan('low')

            elif robot_cmd.command == 'fan-off':
                self.robot_controller.jet_turret.control_fan('off')

            elif robot_cmd.command == 'pump-on':
                self.robot_controller.jet_turret.control_pump('on')

            elif robot_cmd.command == 'pump-off':
                self.robot_controller.jet_turret.control_pump('off')

            elif robot_cmd.command == 'spray':
                fan = robot_cmd.args[0].replace('fan-','')
                tilt = robot_cmd.args[1].replace('tilt-','')
                pump = robot_cmd.args[2].replace('pump-','')
                self.robot_controller.jet_turret.control_fan(fan)
                self.robot_controller.jet_turret.control_tilt(tilt)
                self.robot_controller.jet_turret.control_pump(pump)

        except  Exception as e:
            logger.error(e)
        else:
            #####   stream end보내기 전까지 잠깐 쉼
            logger.info("send stream end")
            time.sleep(0.5)
            self.send_robot_status(stream_end='end')
        finally:
            pass

    def refresh_params(self, param):
        addr = self.config.server_ports['ip_addr']
        port = self.config.server_ports['rest_port']
        serial = self.serial
        logger.debug(param)
        try:

            if param == 0:
                url = f'http://{addr}:{port}/db/drive-param/get/{serial}'
                logger.debug(url)
                response = requests.get(url)
                response.raise_for_status()
                dict = response.json()
                drive_param = DriveParam(**dict)
                logger.debug(f'recv={dict}')
                self.robot_controller.diff_drive.set_param(drive_param)
                self.config.drive_params = drive_param
            elif param == 1:
                url = f'http://{addr}:{port}/db/turret-param/get/{serial}'
                logger.debug(url)
                response = requests.get(url)
                response.raise_for_status()
                dict = response.json()
                turret_param = TurretParam(**dict)
                logger.debug(f'recv={dict}')
                self.robot_controller.jet_turret.set_param(turret_param)
                self.config.turret_params = turret_param
            elif param == 2:
                url = f'http://{addr}:{port}/db/navigation-param/get/{serial}'
                logger.debug(url)
                response = requests.get(url)
                response.raise_for_status()
                dict = response.json()
                logger.debug(f'recv={dict}')
                navigation_param = NavigationParam(**dict)
                self.robot_controller.set_navigation_param(navigation_param)
                self.config.navigation_params = navigation_param

            self.config.save_config()
        except Exception as e:
            logger.error(e)

    def send_robot_status(self, drive_status: DriveStatus = None, navigation_status: NavigationStatus = None,
                          stream_end=None):
        dict = {}
        # logger.debug("send_robot_status")
        # logger.debug(drive_status)
        # logger.debug(navigation_status)
        if navigation_status:
            logger.info(navigation_status)
        if drive_status:
            pass
            # logger.info(drive_status)

        dict['serial'] = self.serial
        dict['timestamp'] = round(time.time() * 1000)
        if drive_status:
            dict['drive_status'] = drive_status.model_dump()
        if navigation_status:
            dict['navigation_status'] = navigation_status.model_dump()
        if stream_end:
            dict['stream_end'] = stream_end
        self.last_robot_status =RobotStatus(**dict)
        self.send_display_data()
        # logger.debug("send json")
        try:
            self.state_request_socket.send_json(dict)
            logger.debug(dict)
            self.state_request_socket.recv_json()
        except Exception as e:
            logger.warning(e)

    def send_display_data(self):
        robot_status = self.last_robot_status
        global global_display_data
        if not robot_status:
            '''1. 최초 연결시 '''
            if self.liquid_level == '부족':
                global_display_data = DisplayData(battery=BATTERY.mid, liquid=self.liquid_level,
                                                  task_status=ERROR.liquid.value,
                                                  task_message="소독액을 보충해주세요")
            else:
                global_display_data = DisplayData(battery=BATTERY.mid, liquid=self.liquid_level,
                                                  task_status=TASK_STATE.ready,
                                                  task_message='연결되었습니다.')
            global_display_data.battery = self.robot_controller.diff_drive.get_battery_level()
            logger.info(global_display_data)

        else:
            # if self.liquid_level == '부족':
            #     global_display_data.task_status = ERROR.liquid.value
            #     global_display_data.task_message = "소독액을 보충해주세요"

            if robot_status.navigation_status:
                if robot_status.navigation_status.error:
                    global_display_data.task_status = robot_status.navigation_status.error
                    global_display_data.task_message = robot_status.navigation_status.msg
                    global_display_data.debug_message = ''
                elif robot_status.navigation_status.status in (
                TASK_STATE.complete, TASK_STATE.action_complete, TASK_STATE.ready):
                    if self.liquid_level == '부족':
                        global_display_data.task_status = ERROR.liquid.value
                        global_display_data.task_message = "소독액을 보충해주세요"
                    else:
                        global_display_data.task_status = robot_status.navigation_status.status
                        global_display_data.task_message = robot_status.navigation_status.msg
                        global_display_data.debug_message = ''
                else:
                    global_display_data.task_status = '소독중'
                    global_display_data.task_message = robot_status.navigation_status.status
                    global_display_data.debug_message = re.sub(pattern=r'\[[^)]*\]'
                                                               , repl='',
                                                               string=robot_status.navigation_status.msg)
            elif robot_status.drive_status:
                if robot_status.drive_status.error:
                    global_display_data.task_status = robot_status.drive_status.error
                    global_display_data.task_message = robot_status.drive_status.msg
                    global_display_data.debug_message = ''
                else:
                    global_display_data.task_status = TASK_STATE.driving
                    global_display_data.task_message = robot_status.drive_status.status
                    global_display_data.debug_message = ''

            # else:
            #     global_display_data.task_status = TASK_STATE.ready.value
            #     global_display_data.task_message = ""

    def prepare_mobile_display_data(self, robot_status: RobotStatus = None):
        try:
            battery_level = self.robot_controller.diff_drive.get_battery_level()
            # 1050 - 100%
            # 870 - 0%
            # range = 120
            # int (100* (raw - 870) / 120))
            range = self.config.drive_params.battery_good_value - self.config.drive_params.battery_normal_value + 20
            battery_percent = int(100 * (20 +
                                         self.robot_controller.diff_drive.get_battery_raw_value() - self.config.drive_params.battery_normal_value) / range)
            if battery_percent > 100:
                battery_percent = 100
            elif battery_percent < 0:
                battery_percent = 0

            if robot_status:
                if robot_status.navigation_status:
                    if robot_status.navigation_status.error:
                        self.task_status = robot_status.navigation_status.error
                    elif robot_status.navigation_status.status in (TASK_STATE.complete, TASK_STATE.action_complete):
                        self.task_status = robot_status.navigation_status.status
                    else:
                        self.task_status = '소독중'
                elif robot_status.drive_status:
                    if robot_status.drive_status.error:
                        self.task_status = robot_status.drive_status.error
                    else:
                        self.task_status = TASK_STATE.driving

            dict = {'robot_serial': self.serial, 'battery_level': battery_level, 'battery_percent': battery_percent,
                    'liquid_level': self.liquid_level, 'task_status': self.task_status, 'temperature': self.temperature,
                    'humidity': self.humidity}
            # logger.info(dict)
            self.mobile_display_data = MobileDisplayData(**dict)
        except Exception as e:
            logger.error(e)
        logger.info(self.mobile_display_data)

    def get_liquid_level(self):
        return "정상"


robot_client = RobotClient(simulation=False)

# 스위치 눌렸을 때 콜백함수

button_activate = False


def stopPressed():
    robot_client.robot_controller.stop_all()
    logger.info("stop all")


def startPressed():
    global batch_task_by_button
    batch_task_by_button = True
    logger.info("request batch-task")


if __name__ == "__main__":
    logger.info("main start")
    if not platform.system() == 'Darwin':
        cv2.namedWindow('title', cv2.WND_PROP_FULLSCREEN)
        cv2.setWindowProperty('title', cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
        logger.info("set full screen")

    if platform.machine() in ('aarch64', 'armv7l'):
        remote_factory = PiGPIOFactory(host='127.0.0.1')
        button = Button(21, pull_up=True, bounce_time=0.2, pin_factory=remote_factory)
        button_stop = Button(3, bounce_time=0.1, pin_factory=remote_factory)
        button.when_pressed = startPressed
        button_stop.when_pressed = stopPressed
    while True:
        if global_display_data:
            try:
                display.set_data(global_display_data)
                image = display.get_image()
                cv2.imshow('title', image)
                if cv2.waitKey(1000) == 27:
                    break
            except Exception as e:
                logger.error(e)

    # while True:
    #     asyncio.run(c.handle_robot_cmd(robot_cmd=robot_cmd))
    #     time.sleep(2)

    # c._cmd_pull_thread.join()
    # logger.debug(e)(c.robot_controller.get_status())
