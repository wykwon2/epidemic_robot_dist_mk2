import numpy as np
from scipy.interpolate import interp1d

x = np.arange(0, 10)
y = np.exp(-x/3.0)


x = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
y = [0, 0, 12, 13, 14, 15, 16, 17, 18, 19]

print(x)
print(y)
pred = interp1d(x, y, kind='linear')
print(pred([0.2]))
