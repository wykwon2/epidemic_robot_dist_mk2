import Adafruit_DHT as dht
import RPi.GPIO as GPIO
import time

button_pin = 11
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM) #핀모드 설정
GPIO.setup(button_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)





while True:
    humidity, temperature = dht.read_retry(dht.DHT22, 10)
    print("Temp={0:0.1f}*C  Humidity={1:0.1f}%".format(temperature, humidity))
    if GPIO.input(button_pin) == GPIO.HIGH:
        print("Button pushed!")

    time.sleep(1)
