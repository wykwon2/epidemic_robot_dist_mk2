import asyncio
import json
import logging
import time
import copy
from collections import deque

import serial
import threading
import numpy as np
import yaml
from scipy.interpolate import interp1d

from ConfigManager import ConfigManager
from dataclass import DeviceStatus, DriveParam, BATTERY, DriveStatus, ERROR
from user_exceptions import StopByUserException, ObstacleException, CmdConflictException
from statistics import mean, stdev

logger = logging.getLogger(__name__)


class DiffDriveController:
    cmd_deque = deque()
    controller_status = {}
    interval = 0.05
    max_obstruction_count = 5
    serial_thread = None
    drive_running = False
    cmd_stop_all = False
    drive_param: DriveParam = None
    sim_dt = 0.1
    yield_div = 5
    prox_window_size = 2
    prox_window_l = deque()
    prox_window_f = deque()
    prox_window_r = deque()

    def __init__(self, drive_serial_port: str, param: DriveParam, simulation=False):
        self.simulation = simulation

        with open('conf/local_conf.yaml', encoding="UTF8") as f:
            dict = yaml.full_load(f)
            self.l_acc = dict['l_acc'] / 1000.0
            self.a_acc = dict['a_acc'] * 3.141592 / 180.0
            self.l_dec = dict['l_dec'] / 1000.0
            self.a_dec = dict['a_dec'] * 3.141592 / 180.0
            self.wheel_balance_move_f = dict['wheel_balance_move_f']
            self.wheel_balance_turn_l = dict['wheel_balance_turn_l']
            self.wheel_balance_turn_r = dict['wheel_balance_turn_r']

            self.min_turn_speed = dict['min_turn_speed']
            self.min_move_speed = dict['min_move_speed']
            self.move_dec_margin = dict['move_dec_margin']
            self.turn_dec_margin = dict['turn_dec_margin']
            self.wheel_balance_move_f = dict['wheel_balance_move_f']
            self.wheel_balance_turn_l = dict['wheel_balance_turn_l']
            self.wheel_balance_turn_r = dict['wheel_balance_turn_r']
            self.move_stop_pred = interp1d(dict['speed_ref'], dict['move_stop_th'], kind='linear')
            self.turn_stop_pred = interp1d(dict['speed_ref'], dict['turn_stop_th'], kind='linear')

            f.close()

        if not self.simulation:
            self.drive_serial_io = serial.Serial(drive_serial_port, baudrate=57600, parity='N', stopbits=1, bytesize=8,
                                                 timeout=8)
            logger.info(f"diff drive connected to {drive_serial_port}")

        self.controller_status = {'time': 0, 'g_x': 0, 'g_y': 0, 'g_heading': 0, 'status': '', 'target': '',
                                  'l_x': 0, 'l_y': 0, 'l_heading': 0, 'l_vel': 0, 'a_vel': 0, 'travel': 0,
                                  'battery': 1000, 'obstruction_cnt': 0, 'l_dist': 0, 'f_dist': 0, 'r_dist': 0}

        time.sleep(2)
        self.set_param(param)
        self.serial_thread = threading.Thread(target=self.serial_io_thread)
        self.serial_thread.start()
        self._start_sensing()
        time.sleep(2)
        logger.info("start sensing in diffdrive")

    def stop_all(self):
        self.cmd_stop_all = True

    def set_param(self, param: DriveParam):
        self.drive_param = param
        self._set_sensing_interval(param.sensing_interval)

    def restart(self):
        self.b_stop = True
        self.serial_thread.join()
        self.serial_thread = threading.Thread(target=self.run)
        self.serial_thread.start()

    def get_status(self):
        status = copy.deepcopy(self.controller_status)
        return status

    def get_battery_raw_value(self):
        if self.controller_status:
            return self.controller_status['battery']

    def get_battery_level(self):
        if self.controller_status:
            v = self.controller_status['battery']
            if v > self.drive_param.battery_good_value:
                return BATTERY.high
            elif v > self.drive_param.battery_normal_value:
                return BATTERY.mid
            else:
                return BATTERY.low

    def get_error_msg(self):
        return self.error_msg

    def detect_obstruction(self):
        threshold = self.drive_param.proximity_dist
        l_dist = self.controller_status['l_dist']
        f_dist = self.controller_status['f_dist']
        r_dist = self.controller_status['r_dist']
        self.prox_window_l.appendleft(l_dist)
        self.prox_window_f.appendleft(f_dist)
        self.prox_window_r.appendleft(r_dist)
        if len(self.prox_window_l) > self.prox_window_size:
            self.prox_window_l.pop()
        if len(self.prox_window_r) > self.prox_window_size:
            self.prox_window_r.pop()
        if len(self.prox_window_f) > self.prox_window_size:
            self.prox_window_f.pop()

        # logger.info(f'l_dist={l_dist}, f_dist={f_dist}, r_dist={r_dist}')
        if mean(self.prox_window_l) > threshold or mean(self.prox_window_r) > threshold or mean(
                self.prox_window_f) > threshold:
            return True
        else:
            return False

    def set_wheel_balance(self, wheel_balance):
        # logger.info(f"pwm-offset {wheel_balance[0]} {wheel_balance[1]}")
        if not self.simulation:
            self.drive_serial_io.write(f"pwm-offset {wheel_balance[0]} {wheel_balance[1]}\n".encode())

    def reset_local_odometry(self):
        if not self.simulation:
            self.drive_serial_io.write("reset local\n".encode())

    def reset_global_odometry(self):
        if not self.simulation:
            self.drive_serial_io.write("reset global\n".encode())

    def get_g_heading_deg(self):
        return self.controller_status['g_heading'] * 180.0 / 3.141592

    async def move(self, distance: float, l_speed=None):
        if not self.simulation:
            self.drive_serial_io.write("reset local\n".encode())
        if not l_speed:
            l_speed = self.drive_param.max_l_speed
        if self.drive_running:
            yield self.write_drive_status(status='error', time=0, target=distance, travel=0, msg='구동부 가동중에 새로운 구동명령 받음',
                                          error=ERROR.conflict)
            raise CmdConflictException()

        else:
            self.drive_running = True

        try:

            start_time = time.time()
            stop_margin = self.move_stop_pred([l_speed])[0]
            # logger.info(stop_margin)
            if distance >= 0:
                l_speed = l_speed * 0.01;
            else:
                l_speed = -l_speed * 0.01;

            if self.simulation:
                self.controller_status['l_x'] = 0


            obstruction_count = 0
            travel_dist = 0
            cur_l_speed = 0
            self.reset_local_odometry()
            self.set_wheel_balance(self.wheel_balance_move_f)

            cnt = 0

            mode = 'accel'
            while True:
                cnt += 1
                ## 정지명령 처리
                if self.cmd_stop_all:
                    raise StopByUserException()
                ## 장애물 처리
                cur_time = time.time() - start_time

                if self.detect_obstruction():
                    obstruction_count += 1
                    yield self.write_drive_status(status='move', time=time.time() - start_time, target=distance,
                                                  travel=travel_dist, msg=f'장애물 감지에 따른 일시정지,{obstruction_count}',
                                                  obstruction_cnt=obstruction_count)
                    logger.warning(f'현재시간={cur_time}\t장애물감지=True\t이동거리={round(travel_dist,2)}cm\t속도={round(cur_l_speed,2)}')
                    self.stop()
                    time.sleep(self.interval)
                    if obstruction_count > self.max_obstruction_count:
                        logger.warning(
                            f'현재시간={round(cur_time, 2)}\t장애물감지=True\t이동거리={round(travel_dist, 2)}cm\t속도={round(cur_l_speed, 2)}')
                        time.sleep(1)
                        logger.warning(
                            f'현재시간={round(cur_time, 2)}\t장애물감지=True\t이동거리={round(travel_dist, 2)}cm\t속도={round(cur_l_speed, 2)}')

                        raise ObstacleException("장애물 감지로 인한 에러")
                    continue

                logger.info(f'현재시간={round(cur_time,2)}\t장애물감지=False\t이동거리={round(travel_dist,2)}cm\t속도={round(cur_l_speed,2)}')

                travel_dist = self.controller_status['l_x'] * 100.0

                if (abs(distance) - abs(travel_dist)) < stop_margin:
                    break
                elif abs(travel_dist) > abs(distance):
                    break

                ##  속도계산
                if abs(cur_l_speed) < abs(l_speed):
                    cur_l_speed += np.sign(l_speed) * self.l_acc * self.interval
                elif abs(travel_dist) > abs(distance) - self.move_dec_margin:
                    cur_l_speed = self.min_move_speed
                else:
                    cur_l_speed = l_speed

                if mode == 'accel':
                    cur_l_speed += np.sign(l_speed) * self.l_acc * self.interval
                    if abs(cur_l_speed) > abs(l_speed):
                        mode = 'drive'
                elif mode == 'drive':
                    cur_l_speed = l_speed
                    if abs(travel_dist) > abs(distance) - self.move_dec_margin:
                        mode = 'decel'
                elif mode == 'decel':
                    cur_l_speed -= np.sign(l_speed) * self.a_acc * self.interval
                    if (abs(cur_l_speed) < abs(self.min_move_speed)) or (np.sign(cur_l_speed) != np.sign(l_speed)):
                        cur_l_speed = np.sign(l_speed) * self.min_move_speed
                        mode = 'trim'
                elif mode == 'trim':
                    cur_l_speed = np.sign(l_speed) * self.min_move_speed

                # logger.info(f'{mode} speed={cur_l_speed}')

                if self.simulation:
                    self.controller_status['l_x'] += cur_l_speed * self.interval
                else:
                    # logger.info(f"setvel {cur_l_speed} {0}\n")
                    self.drive_serial_io.write(f"setvel {cur_l_speed} {0}\n".encode())

                if cnt % self.yield_div == 0:
                    yield self.write_drive_status(status='move', time=time.time() - start_time, target=distance,
                                                  travel=travel_dist, msg='정상동작',
                                                  obstruction_cnt=obstruction_count)

                time.sleep( self.interval)


        except StopByUserException as e:
            self.cmd_stop_all = False
            yield self.write_drive_status(status='error', time=time.time() - start_time, target=distance,
                                          travel=travel_dist, msg='사용자 정지에 따른 중단',
                                          error=ERROR.stop,
                                          obstruction_cnt=obstruction_count)
        except ObstacleException as e:
            yield self.write_drive_status(status='error', time=time.time() - start_time, target=distance,
                                          travel=travel_dist, msg='장애물 감지로 인한 작업중단',
                                          error=ERROR.obstacle,
                                          obstruction_cnt=obstruction_count)
        else:
            yield self.write_drive_status(status='ready', time=time.time() - start_time, target=distance,
                                          travel=travel_dist, msg='이동종료',
                                          obstruction_cnt=obstruction_count)
        finally:
            self.stop()
            self.reset_local_odometry()
            self.drive_running = False

    async def turn(self, degree: float, a_speed=None):
        if not self.simulation:
            self.drive_serial_io.write("reset local\n".encode())
        if not a_speed:
            a_speed = self.drive_param.max_a_speed

        if self.drive_running:
            yield self.write_drive_status(status='error', time=0, target=degree, travel=0, msg='구동부 가동중에 새로운 구동명령 받음',
                                          error=ERROR.conflict)
            raise CmdConflictException()
        else:
            self.drive_running = True

        try:
            if self.simulation:
                self.controller_status['l_heading'] = 0

            start_time = time.time()

            stop_margin = self.turn_stop_pred([a_speed])[0]
            if degree >= 0:
                a_speed = np.deg2rad(a_speed);
            else:
                a_speed = -np.deg2rad(a_speed);

            travel_angle = 0
            # heading0 = np.rad2deg(self.controller_status['l_heading'])
            cur_a_speed = 0
            cnt = 0
            self.reset_local_odometry()
            if degree > 0:
                self.set_wheel_balance(self.wheel_balance_turn_l)
            else:
                self.set_wheel_balance(self.wheel_balance_turn_r)

            mode = 'accel'

            while True:
                cnt += 1
                if self.cmd_stop_all:
                    raise StopByUserException()

                travel_angle = np.rad2deg(self.controller_status['l_heading'])

                if mode == 'accel':
                    cur_a_speed += np.sign(a_speed) * self.a_acc * self.interval
                    if abs(cur_a_speed) > abs(a_speed):
                        mode = 'drive'
                elif mode == 'drive':
                    cur_a_speed = a_speed
                    if abs(travel_angle) > abs(degree) - self.turn_dec_margin:
                        mode = 'decel'
                elif mode == 'decel':
                    cur_a_speed -= np.sign(a_speed) * self.a_acc * self.interval
                    if abs(cur_a_speed) < abs(self.min_turn_speed) or np.sign(a_speed) != np.sign(cur_a_speed):
                        cur_a_speed = np.sign(a_speed) * self.min_turn_speed
                        mode = 'trim'
                        logger.info(f'{mode} speed={cur_a_speed}  ')
                elif mode == 'trim':
                    cur_a_speed = np.sign(a_speed) * self.min_turn_speed

                if (abs(degree) - travel_angle) < stop_margin:
                    break
                if abs(travel_angle) > abs(degree):
                    break

                if not self.simulation:
                    self.drive_serial_io.write(f"setvel {0} {cur_a_speed}\n".encode())

                if cnt % self.yield_div == 0:
                    yield self.write_drive_status(status='turn', time=time.time() - start_time, target=degree,
                                                  travel=travel_angle, msg='Turn 작업중 ')

                cur_time = time.time() - start_time
                logger.info(
                    f'현재시간={round(cur_time, 2)}\t이동각도={round(travel_angle, 2)}도\t속도={round(cur_a_speed, 2)}')


                if self.simulation:
                    self.controller_status['l_heading'] += 0.5 * self.sim_dt * a_speed

                time.sleep(self.interval)



        except StopByUserException as e:
            self.cmd_stop_all = False
            yield self.write_drive_status(status='error', time=time.time() - start_time, target=degree,
                                          travel=travel_angle, msg='사용자 정지에 따른 중단',
                                          error=ERROR.stop)
        except ObstacleException as e:
            yield self.write_drive_status(status='error', time=time.time() - start_time, target=degree,
                                          travel=travel_angle, msg='장애물 감지로 인한 작업중단',
                                          error=ERROR.obstacle)
        else:
            yield self.write_drive_status(status='ready', time=time.time() - start_time, target=degree,
                                          travel=travel_angle, msg='이동종료')

        finally:
            self.stop()
            self.reset_local_odometry()
            self.drive_running = False

    def write_drive_status(self, time, status, target, travel,
                           obstruction_cnt=0, error="", msg=""):
        digit = 4
        self.controller_status['time'] = round(time, digit)
        self.controller_status['status'] = status
        self.controller_status['target'] = round(target, digit)
        self.controller_status['travel'] = round(travel, digit)

        if obstruction_cnt > 0:
            self.controller_status['obstruction_cnt'] = obstruction_cnt
        if error is not None:
            self.controller_status['error'] = error
        if msg is not None:
            self.controller_status['msg'] = msg

        return DriveStatus(**self.controller_status)

    def stop(self):
        if not self.simulation:
            self.drive_serial_io.write("setvel 0 0\n".encode())
            time.sleep(0.1)
            # self.drive_serial_io.write("reset local\n".encode())
            # time.sleep(0.5)

    def _start_sensing(self):
        if not self.simulation:
            self.drive_serial_io.write("start-sensing\n".encode())

    def _stop_sensing(self):
        if not self.simulation:
            self.drive_serial_io.write("stop-sensing\n".encode())

    def _set_sensing_interval(self, interval_sec: float):
        self.interval = interval_sec
        interval_ms = int(interval_sec * 1000)
        if not self.simulation:
            self.drive_serial_io.write(f"interval {50}\n".encode())
        # self.drive_serial_io.write(f"interval {interval_ms}\n".encode())

    def serial_io_thread(self):
        logger.info("serial io thread start")
        old_cmd = ''
        while True:
            if not self.simulation:
                try:
                    message = self.drive_serial_io.readline()
                    message = message.decode('utf-8').strip()
                    packet = message.split(",")

                    if len(packet) == 12:
                        self.controller_status['g_x'] = float(packet[0])
                        self.controller_status['g_y'] = float(packet[1])
                        self.controller_status['g_heading'] = float(packet[2])
                        self.controller_status['l_x'] = float(packet[3])
                        self.controller_status['l_y'] = float(packet[4])
                        self.controller_status['l_heading'] = float(packet[5])
                        self.controller_status['l_vel'] = float(packet[6])
                        self.controller_status['a_vel'] = float(packet[7])
                        self.controller_status['battery'] = float(packet[8])
                        self.controller_status['l_dist'] = float(packet[9])
                        self.controller_status['f_dist'] = float(packet[10])
                        self.controller_status['r_dist'] = float(packet[11])
                        self.controller_status['error'] = ""
                        # logger.info(self.controller_status)
                    else:
                        logger.info(f"in diff drive controller: wrong packet {message} size={len(packet)}")

                except Exception as e:
                    logger.error(e)


async def main():
    config = ConfigManager()
    config.load_config()
    # drive = DiffDriveController(drive_serial_port="/dev/ttyUSB0", param=config.drive_params, simulation=False)
    drive = DiffDriveController(drive_serial_port="COM11", param=config.drive_params, simulation=False)

    logger.info("init")

    async for event in drive.move(50):
        print(event)


if __name__ == "__main__":
    asyncio.run(main())
